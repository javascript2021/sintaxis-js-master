// PARA COMENTARIO DE UNA SOLA LINEA, DOBLE BARRA INCLINADA
// Autor: Julio
// Ubicación: Buenos Aires, Argentina

/* ESTE CONJUNTO DE CARACTERES QUE PRECEDE SON PARA COMENTARIO DE BLOQUE,
FORMAS DE DEFINIR VARIABLES Y CONSTANTES EN JAVASCRIPT
*/

let nombre = "Julio";   // PARA PONER EN PARTE DE LA LINEA, DOBLE BARRA INCLINADA
console.log(nombre);
nombre = "María";
console.log(nombre);

const VELOCIDAD_MAXIMA = 120;

let _edad = 35;
let edad = 37;
let $apellido = "D'Antoni";
console.log(_edad, edad, $apellido);
console.log(VELOCIDAD_MAXIMA);

